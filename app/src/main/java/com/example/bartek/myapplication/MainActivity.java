package com.example.bartek.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    List <String> lista  = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
mTrueButton = findViewById(R.id.mTrueButton);
mFalseButton=findViewById(R.id.mFalseButton);
mNextButton=findViewById(R.id.mNextButton);
mQuestionTextView=findViewById(R.id.textView);
mQuestionTextView.setText(mQuestionBank[mCurrentIndex].getTextResId());

    }
    void updateQuestion(){
        mCurrentIndex++;
        if (mCurrentIndex >= mQuestionBank.length ){
            mCurrentIndex=0;
        }
    }
    void checkAnswer(boolean userPressedTrue) {
        int messageToDisplay;

        if (mQuestionBank[mCurrentIndex].isAnswerTrue() == userPressedTrue) {
            messageToDisplay = R.string.true_toast;
        } else {
            messageToDisplay = R.string.false_toast;
        }
    }

     Button mTrueButton ;
     Button mFalseButton ;
     Button mNextButton;
     TextView mQuestionTextView;
     int mCurrentIndex = 0 ;
    Question[] mQuestionBank = new Question[]{
            new Question(R.string.question_sniezka,true),
            new Question(R.string.question_stolica_dolnego,true),
            new Question(R.string.question_wisla,false),
             };



}


